package williamhill.retail;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void alwaysTrue() {
        assertThat(true, CoreMatchers.is(true));
    }

}
